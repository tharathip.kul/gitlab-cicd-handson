from fastapi import FastAPI
import uvicorn
from models.routes.root.root_request import Name
from models.global_response import GlobalResponse

app = FastAPI()

@app.post("/", name="root_PROD")
async def test_root(input: Name) -> GlobalResponse:
    response = GlobalResponse(
        success=True,
        message=f"Welcome to TCC DS Team.\n Hello, {input.name}",
        data={"name": input.name}
    )
    return response

if __name__ == "__main__":
    uvicorn.run("app:app", host="0.0.0.0", port=8000, log_level="info")