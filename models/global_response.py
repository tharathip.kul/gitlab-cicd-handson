from pydantic import BaseModel

class GlobalResponse(BaseModel):
    success: bool
    message: str
    data: dict