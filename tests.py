from fastapi.testclient import TestClient
from app import app

client = TestClient(app)

def test_root():
    body = {"name": "Test"}
    response = client.post("/", json=body).json()
    assert response['success'] == True
    assert response['message'] == "Welcome to TCC DS Team.\n Hello, Test"