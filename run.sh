#!/bin/bash
source ~/.virtualenv/bin/activate

nohup python3 app.py > uvicorn.log 2>&1 || true